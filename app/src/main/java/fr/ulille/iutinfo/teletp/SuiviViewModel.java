package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    private MutableLiveData<Integer> liveNextQuestion;
    private String[] questions;


    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        liveNextQuestion = new MutableLiveData<Integer>(0);

    }
    void initQuestions(Context context){
        this.questions = context.getResources().getStringArray(R.array.list_questions);
    }

    String getQuestions(int position){
        if(position > -1 && position < questions.length-1){
        return questions[position];
        }
        return null;
    }

    LiveData<Integer> getLiveNextQuestion(){
        return liveNextQuestion;
    }


    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    void setNextQuestion(Integer nextQuestion){
        liveNextQuestion.postValue(nextQuestion);
    }

    Integer getNextQuestion(){
        return this.liveNextQuestion.getValue();
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    

}
