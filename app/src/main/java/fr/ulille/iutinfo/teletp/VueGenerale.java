package fr.ulille.iutinfo.teletp;

import android.graphics.ColorSpace;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {


    private static String DISTANCIEL = null;
    private String salle;

    private String poste = "";
    private SuiviViewModel model;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        this.DISTANCIEL = (getResources().getStringArray(R.array.list_salles))[0];
        this.salle = this.DISTANCIEL ;

        this.model = MainActivity.suiviModel;


        // TODO Q4

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView login = view.findViewById(R.id.tvLogin);
            this.model.setUsername(login.getText().toString());

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        // TODO Q9
    }

    // TODO Q5.a
    // TODO Q9
}